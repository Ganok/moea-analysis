========================================================================
PISA  (www.tik.ee.ethz.ch/pisa/)
========================================================================
Computer Engineering (TIK)
ETH Zurich	 
========================================================================
SEMO - Simple Evolutionary Multi-objective Optimizer

Example implementation in C for the selection side.

Documentation
  
file: semo_documentation.txt
author: Stefan Bleuler, bleuler@tik.ee.ethz.ch
last change: $date$
========================================================================

The Optimizer
=============

Caution: This version of SEMO is just a demonstration program for the
PISA interface it can only handle very restricted parameter settings.

SEMO is a simple evoluationary multiobjective optimizer. SEMO
maintains an archive of all non-dominated solutions. A parent
individual is chosen uniformly from this archive. A newly variated
individual is only added to the archive if it is not dominated by any
other individual in the archive and if it is not equal in all
objective values to any other individual in the archive.


The Parameters
==============

SEMO uses the following values for the common parameters:

alpha 1  (size of the initial population)
mu 1     (number of parent individuals)
lambda 1 (number of offspring individuals)
dim 2    (number of objectives)

'PISA_cfg' is a PISA_configuration file.

SEMO takes one local parameter which is given in a parameter file. The
name of this parameter file is passed to SEMO as command line argument.

seed     (seed for the random number generator)

'semo_param.txt' is a PISA_parameter file.


Source Files
============

The source code for SEMO is divided into four files:

'semo.h' is the header file.

'semo.c' contains the main function and implements the control flow.

'semo_io.c' implements the file i/o functions.

'semo_functions.c' implements all other functions including the
selection.

Additionally a Makefile, a PISA_configuration file with common
parameters and a PISA_parameter file with local parameters are
contained in the tar file.

For compiling on Windows and Unix (any OS having <unistd.h>) uncomment
the according '#define' in the 'semo.h' file.


Usage
=====

Call SEMO with the following arguments:

lotz paramfile filenamebase poll

paramfile: specifies the name of the file containing the local
parameters (e.g. semo_param.txt)

filenamebase: specifies the name (and optionally the directory) of the
communication files. The filenames of the communication files and the
configuration file are built by appending 'sta', 'var', 'sel','ini',
'arc' and 'cfg' to the filenamebase. This gives the following names for
the 'PISA_' filenamebase:

PISA_cfg - configuration file
PISA_ini - initial population
PISA_sel - individuals selected for variation (parents)
PISA_var - variated individuals (offspring)
PISA_arc - individuals in the archive


Caution: the filenamebase must be consistent with the name of
the configuration file and the filenamebase specified for the variator
module.

poll: gives the value for the polling time in seconds (e.g. 0.5). This
      polling time must be larger than 0.01 seconds.


Limitations
===========

The maximum number of individuals generated in a whole run is:
100'000.  If this limit is exceeded an error message is generated and
SEMO is terminated.


Stopping and Resetting
======================

The behaviour in state 5 and 9 is not determined by the interface but
by each variator module specifically. SEMO behaves as follows:

state 5 (= variator terminated): set state to 6 (terminate as well).
state 9 (= variator resetted): set state to 10 (reset as well).
