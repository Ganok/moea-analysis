function plotHisto

    FRONTDIR = '../pf/';
    DIR = './compare/sets/';
    OUTDIR = './compare/plot_sets/';
    files = dir([DIR '*.sets']);
    bins = 100;

    figure('visible','off');
    for file = files'
        %format name nicely
        name = strrep(strtok(file.name, '.'), '_', ' ');

        A = readtable([DIR,file.name],'FileType','text','ReadVariableNames',false,'Delimiter','\t');
        A = table2array(A);

        pfname = strtok(file.name,'_');
        if ~isempty(strfind(file.name,'_2'))
            pfname = [pfname '.2D'];
        elseif ~isempty(strfind(file.name,'DTLZ')) || ~isempty(strfind(file.name,'WFG')) % >2 dim
            continue;
        end
        pf = readtable([FRONTDIR,pfname,'.pf'],'FileType','text','ReadVariableNames',false,'Delimiter',' ');
        pf = table2array(pf);

        if size(A,2)==1
            histogram(A,bins);
            if strfind(file.name,'obj')
                yL = get(gca,'YLim');
                hold on
                line([min(pf) min(pf)],yL,'Color','r');
                line([max(pf) max(pf)],yL,'Color','r');
                hold off
            end

            title(name);
            print([OUTDIR strrep(name,' ','_')],'-dpng');

        elseif size(A,2)==2
            histogram2(A(:,1), A(:,2), [bins bins]);
            if strfind(file.name,'obj')
                hold on
                scatter(pf(:,1), pf(:,2), 30,'filled');
                hold off
            end

            title(name);
            print([OUTDIR strrep(name,' ','_')],'-dpng');
        end

    end

end

