import java.util.List;
import java.io.File;

import org.moeaframework.Executor;
import org.moeaframework.Analyzer;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;

public class IBEAParam {

	private static void usage() {
		System.err.println("Usage: IBEAParam " +
								"-p PROBLEM_NAME -s POPULATION_SIZE " +
								"-m MAX_FUNCS_EVAL -r NUMBER_SEEDS " +
								"-sbxr SBX.RATE -sbxd SBX.DISTRIDX " +
								"-pmidx PM.DISTRIDX -ind INDICATOR " +
								"-printvar -printobj");
		System.exit(-1);
	}

	public static void main(String[] args) {
		String algName = "IBEA";
		String problemName = "Schaffer";
		int seeds = 100, mfe = 10000, populSize = 100;
		double sbxrate = 1.0, sbxidx = 15.0, pmidx = 20.0;
		boolean printvar = false, printobj = false;
		String ind = "hypervolume";

		//parse arguments to set options
		int n = args.length;
		for (int i = 0; i < n; ++i) {
			if (args[i].equals("-sbxr") && i+1 < n)
				sbxrate = Double.valueOf(args[++i]);
			else if (args[i].equals("-sbxd") && i+1 < n)
				sbxidx = Double.valueOf(args[++i]);
			else if (args[i].equals("-printvar"))
				printvar = true;
			else if (args[i].equals("-printobj"))
				printobj = true;
			else if (args[i].equals("-pmidx") && i+1 < n)
				pmidx = Double.valueOf(args[++i]);
			else if (args[i].equals("-ind") && i+1 < n)
				ind = args[++i];
			else if (args[i].equals("-p") && i+1 < n)
				problemName = args[++i];
			else if (args[i].equals("-s") && i+1 < n)
				populSize = Integer.parseInt(args[++i]);
			else if (args[i].equals("-m") && i+1 < n)
				mfe = Integer.parseInt(args[++i]);
			else if (args[i].equals("-r") && i+1 < n)
				seeds = Integer.parseInt(args[++i]);
			else usage();
		}

		File f = new File("./tests/param_tuning/" + algName + "_" +
			   problemName + "_" + String.valueOf(seeds));

		Analyzer analyzer = new Analyzer()
			.withProblem(problemName)
			.includeInvertedGenerationalDistance()
			.includeSpacing()
			.includeGeneralizedSpread()
			.includeR2()
			.includeAdditiveEpsilonIndicator()
			.includeHypervolume()
			.showStatisticalSignificance();

		Executor newparams = new Executor()
				.withProblem(problemName)
				.withProperty("populationSize", populSize)
				.withProperty("sbx.rate", sbxrate)
				.withProperty("sbx.distributionIndex", sbxidx)
				.withProperty("pm.distributionIndex", pmidx)
				.withProperty("indicator", ind)
				.withMaxEvaluations(mfe)
				.withAlgorithm(algName)
				.distributeOnAllCores();

		Executor defaultparams = new Executor()
				.withProblem(problemName)
				.withProperty("populationSize", populSize)
				.withMaxEvaluations(mfe)
				.withAlgorithm(algName)
				.distributeOnAllCores();

		List<NondominatedPopulation> result = newparams.runSeeds(seeds);

		if (printvar) {
			for (NondominatedPopulation pop : result) {
				for (Solution solution : pop) {
					System.out.format("%.10f", // decimals can modify results!
							EncodingUtils.getReal(solution.getVariable(0)));
					for (int i = 1; i < solution.getNumberOfVariables(); ++i) {
						System.out.format("\t%.10f",
								EncodingUtils.getReal(solution.getVariable(i)));
					}
					System.out.println();
				}
			}
		}
		else if (printobj) {
			for (NondominatedPopulation pop : result) {
				for (Solution solution : pop) {
					System.out.format("%.10f",
							solution.getObjective(0));
					for (int i = 1; i < solution.getNumberOfObjectives(); ++i) {
						System.out.format("\t%.10f",
								solution.getObjective(i));
					}
					System.out.println();
				}
			}
		}

		analyzer.addAll("New " + algName, result);
		analyzer.addAll("Default " + algName, defaultparams.runSeeds(seeds));

		try {
			analyzer.saveAnalysis(f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

