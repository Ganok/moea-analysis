#!/bin/bash

default=$2_$1_50_def
tuned=$2_$1_50_tuned
java -cp "lib/*:class" $1Param -r 50 -p $2 -printobj > ./tests/param_tuning/${default}.sets
java -cp "lib/*:class" $1Param -r 50 -p $2 ${@:3} -printobj > ./tests/param_tuning/${tuned}.sets
/home/guido/Software/MATLAB/bin/matlab -nodisplay -nosplash -r "cd tests;plot_tuning;exit;"
cd ./tests/param_tuning/histograms
convert ${default}.png -trim -bordercolor White -border 5x5 +repage ${default}.png
convert ${tuned}.png -trim -bordercolor White -border 5x5 +repage ${tuned}.png
montage -geometry +4+4 ${default}.png ${tuned}.png ${2}_${1}.png
rm ${default}.png ${tuned}.png
rm ../*.sets

