import java.util.List;
import java.io.File;

import org.moeaframework.Executor;
import org.moeaframework.Analyzer;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;

public class MOEADParam {

	private static void usage() {
		System.err.println("Usage: MOEADParam " +
								"-p PROBLEM_NAME -s POPULATION_SIZE " +
								"-m MAX_FUNCS_EVAL -r NUMBER_SEEDS " +
								"-der DE.RATE -des DE.STEPSIZE " +
								"-pmidx PM.DISTRIDX -printvar -printobj " +
								"-ns NEIGHBORHOOD_SIZE -delta -eta -uu UPDATEUTILITY");
		System.exit(-1);
	}

	public static void main(String[] args) {
		String algName = "MOEAD";
		String problemName = "Schaffer";
		int seeds = 100, mfe = 10000, populSize = 100, updUtility = -1;
		double derate = 0.1, stepsize = 0.5, pmidx = 20.0;
		double neighsize = 0.1, delta = 0.9, eta = 0.01;
		boolean printvar = false, printobj = false, utility = false;

		//parse arguments to set options
		int n = args.length;
		for (int i = 0; i < n; ++i) {
			if (args[i].equals("-der") && i+1 < n)
				derate = Double.valueOf(args[++i]);
			else if (args[i].equals("-des") && i+1 < n)
				stepsize = Double.valueOf(args[++i]);
			else if (args[i].equals("-printvar"))
				printvar = true;
			else if (args[i].equals("-printobj"))
				printobj = true;
			else if (args[i].equals("-pmidx") && i+1 < n)
				pmidx = Double.valueOf(args[++i]);
			else if (args[i].equals("-ns") && i+1 < n)
				neighsize = Double.valueOf(args[++i]);
			else if (args[i].equals("-delta") && i+1 < n)
				delta = Double.valueOf(args[++i]);
			else if (args[i].equals("-eta") && i+1 < n)
				eta = Double.valueOf(args[++i]);
			else if (args[i].equals("-uu") && i+1 < n) {
				updUtility = Integer.parseInt(args[++i]);
				utility = true;
			}
			else if (args[i].equals("-p") && i+1 < n)
				problemName = args[++i];
			else if (args[i].equals("-s") && i+1 < n)
				populSize = Integer.parseInt(args[++i]);
			else if (args[i].equals("-m") && i+1 < n)
				mfe = Integer.parseInt(args[++i]);
			else if (args[i].equals("-r") && i+1 < n)
				seeds = Integer.parseInt(args[++i]);
			else usage();
		}

		File f = new File("./tests/param_tuning/" + algName + "_" +
			   problemName + "_" + String.valueOf(seeds));

		Analyzer analyzer = new Analyzer()
			.withProblem(problemName)
			.includeInvertedGenerationalDistance()
			.includeSpacing()
			.includeGeneralizedSpread()
			.includeR2()
			.includeAdditiveEpsilonIndicator()
			.includeHypervolume()
			.showStatisticalSignificance();

		Executor newparams = new Executor()
				.withProblem(problemName)
				.withProperty("populationSize", populSize)
				.withProperty("de.crossoverRate", derate)
				.withProperty("de.stepSize", stepsize)
				.withProperty("pm.distributionIndex", pmidx)
				.withProperty("neighborhoodsize", neighsize)
				.withProperty("delta", delta)
				.withProperty("eta", eta)
				.withMaxEvaluations(mfe)
				.withAlgorithm(algName)
				.distributeOnAllCores();

		if (utility) newparams.withProperty("updateUtility", updUtility);

		Executor defaultparams = new Executor()
				.withProblem(problemName)
				.withProperty("populationSize", populSize)
				.withMaxEvaluations(mfe)
				.withAlgorithm(algName)
				.distributeOnAllCores();

		List<NondominatedPopulation> result = newparams.runSeeds(seeds);

		if (printvar) {
			for (NondominatedPopulation pop : result) {
				for (Solution solution : pop) {
					System.out.format("%.10f", // decimals can modify results!
							EncodingUtils.getReal(solution.getVariable(0)));
					for (int i = 1; i < solution.getNumberOfVariables(); ++i) {
						System.out.format("\t%.10f",
								EncodingUtils.getReal(solution.getVariable(i)));
					}
					System.out.println();
				}
			}
		}
		else if (printobj) {
			for (NondominatedPopulation pop : result) {
				for (Solution solution : pop) {
					System.out.format("%.10f",
							solution.getObjective(0));
					for (int i = 1; i < solution.getNumberOfObjectives(); ++i) {
						System.out.format("\t%.10f",
								solution.getObjective(i));
					}
					System.out.println();
				}
			}
		}

		analyzer.addAll("New " + algName, result);
		analyzer.addAll("Default " + algName, defaultparams.runSeeds(seeds));

		try {
			analyzer.saveAnalysis(f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

