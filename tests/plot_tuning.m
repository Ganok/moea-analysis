function plot_tuning

    DIR = './param_tuning/';
    OUTDIR = './param_tuning/histograms/';
    files = dir([DIR '*.sets']);
    bins = 100;

    figure('visible','off');
    for file = files'
        %format name nicely
        name = strrep(strtok(file.name, '.'), '_', ' ');

        A = readtable([DIR,file.name],'FileType','text','ReadVariableNames',false,'Delimiter','\t');
        A = table2array(A);

        if size(A,2)==1
            histogram(A,bins);
            title(name);
            print([OUTDIR strrep(name,' ','_')],'-dpng');

        elseif size(A,2)==2
            histogram2(A(:,1), A(:,2), [bins bins]);
            title(name);
            print([OUTDIR strrep(name,' ','_')],'-dpng');
        end

    end

end

