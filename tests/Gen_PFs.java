import java.util.List;

import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;

public class Gen_PFs {

	private static void usage() {
		System.err.println("Usage: Gen_PFs -a ALG_NAME " +
								"-p PROBLEM_NAME -s POPULATION_SIZE " +
								"-m MAX_FUNCS_EVAL -r NUMBER_SEEDS\n");
		System.exit(-1);
	}

	public static void main(String[] args) {
		String algName = "VEGA", problemName = "Schaffer";
		int seeds = 100, mfe = 10000, populSize = 100;

		//parse arguments to set options
		int n = args.length;
		for (int i = 0; i < n; ++i) {
			if (args[i].equals("-a") && i+1 < n)
				algName = args[++i];
			else if (args[i].equals("-p") && i+1 < n)
				problemName = args[++i];
			else if (args[i].equals("-s") && i+1 < n)
				populSize = Integer.parseInt(args[++i]);
			else if (args[i].equals("-m") && i+1 < n)
				mfe = Integer.parseInt(args[++i]);
			else if (args[i].equals("-r") && i+1 < n)
				seeds = Integer.parseInt(args[++i]);
			else usage();
		}

		//configure and run this experiment
		List<NondominatedPopulation> result = new Executor()
				.withProblem(problemName)
				.withAlgorithm(algName)
				.withProperty("populationSize", populSize)
				.withMaxEvaluations(mfe)
				.distributeOnAllCores()
				.runSeeds(seeds);

		for (NondominatedPopulation pop : result) {
			for (Solution solution : pop) {
				System.out.format("%.10f", // decimals can modify results!
						EncodingUtils.getReal(solution.getVariable(0)));
				for (int i = 1; i < solution.getNumberOfVariables(); ++i) {
					System.out.format("\t%.10f",
							EncodingUtils.getReal(solution.getVariable(i)));
				}
				System.out.println();
			}
		}
	}
}

