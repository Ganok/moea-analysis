#!/bin/bash

POPULATION=100
NSEEDS=100
MFE=25000
PROBLEMS=( Fonseca Schaffer Schaffer2 DTLZ1_2 DTLZ2_2 DTLZ3_2 DTLZ4_2 DTLZ7_2 WFG1_2 WFG2_2 WFG3_2 WFG4_2 WFG5_2 WFG6_2 WFG7_2 WFG8_2 WFG9_2 )
ALGORITHMS=( NSGAII VEGA NSGAIII SPEA2 SMPSO OMOPSO MOEAD eMOEA eNSGAII PAES PESA2 AbYSS CellDE MOCell )
DIRSETS=${PWD}/tests/fronts
DIRMETRICS=${PWD}/tests/metrics
DIRDEFAULT=${DIRMETRICS}/default
DIR=${DIRSETS}
PROG="Gen_PFs"
EXTENSION=( "sets" "metrics" )
EXT=0

# JAVA_ARGS="-Djava.ext.dirs=lib -Xmx512m"
JAVA_ARGS="-cp examples:lib/*:tests"

mkdir -p ${PWD}/tests/metrics/default

if [[ $* == *-default* ]]; then
	DIR=${DIRDEFAULT}
	EXT=1
	PROG="EvalDefParams"
	echo -n "Generating metrics with default parameters..."
else
	echo -n "Genereting Pareto Fronts with default parameters..."
fi

# Clear old data
echo -n "Clearing old data..."
for ALGORITHM in ${ALGORITHMS[@]}
do
	for PROBLEM in ${PROBLEMS[@]}
	do
		rm -f ${DIR}/${ALGORITHM}_${PROBLEM}*.${EXTENSION[${EXT}]}
	done
done
echo "done."

# Run algorithms for each problem 
for ALGORITHM in ${ALGORITHMS[@]}
do
	for PROBLEM in ${PROBLEMS[@]}
	do
		echo -n "Running" ${ALGORITHM} "with problem" ${PROBLEM} "... "
		java ${JAVA_ARGS} ${PROG} -a ${ALGORITHM} -p ${PROBLEM} -s ${POPULATION} -m ${MFE} -r ${NSEEDS} > ${DIR}/${ALGORITHM}_${PROBLEM}_${NSEEDS}.${EXTENSION[${EXT}]}
		echo "done."
	done
done

