function plot_funcs

    % Fonseca 1
    f1 = @(x,y) 1 - exp(-(x-1)^2 - (y+1)^2);
    f2 = @(x,y) 1 - exp(-(x+1)^2 - (y-1)^2);
    ezsurf(f1, [-1 1]);
    hold on
    ezsurf(f2,[-1 1]);
    hold off
    
 
    % Fonseca 2
    figure;
    f1 = @(x,y) 1 - exp(-sum((x-(1/sqrt(length(x))))^2));
    f2 = @(x,y) 1 - exp(-sum((x+(1/sqrt(length(x))))^2));
    ezsurf(f1, [-1 1]);
    hold on
    ezsurf(f2,[-1 1]);
    hold off
    
    % Schaffer1
    figure;
    f1 = @(x) x^2;
    f2 = @(x) (x-2)^2;
    fplot(f1, [-2 4]);
    hold on
    fplot(f2,[-2 4]);
    area([0 2], [18 18], 'FaceAlpha',0.25,'FaceColor','b','LineStyle','--','EdgeAlpha',0.0,'ShowBaseLine','off');
    axis([-2 4 0 16]);
    legend('f_1','f_2','Location','ne');
    hold off
    
end

