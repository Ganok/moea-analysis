function plotTimes

    DIR = './compare/time/';
    OUTDIR = './compare/plot_times/';
    files = dir([DIR '*.time']);

    fig = figure('visible','off');
    fig.PaperUnits = 'inches';
    fig.PaperPosition = [0 0 12 6];
    for file = files'
        %format name nicely
        name = strrep(strtok(file.name, '.'), '_', ' ');

        T = readtable([DIR,file.name],'FileType','text','ReadVariableNames',false,'Delimiter',' ');
        bar(T.Var2);
        title([name ' Execution Times']);
        ylabel('Seconds');
        set(gca,'Xtick',1:length(T.Var2),'XTickLabel',T.Var1','FontSize',9)
        print([OUTDIR strrep(name,' ','_')],'-dpng','-r0');
    end

end

