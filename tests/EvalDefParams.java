import java.util.List;

import org.moeaframework.Executor;
import org.moeaframework.Analyzer;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;

public class EvalDefParams {

	private static void usage() {
		System.err.println("Usage: EvalDefParams -a ALG_NAME " +
								"-p PROBLEM_NAME -s POPULATION_SIZE " +
								"-m MAX_FUNCS_EVAL -r NUMBER_SEEDS\n");
		System.exit(-1);
	}

	public static void main(String[] args) {
		String algName = "VEGA", problemName = "Schaffer";
		int seeds = 100, mfe = 10000, populSize = 100;

		//parse arguments to set options
		int n = args.length;
		for (int i = 0; i < n; ++i) {
			if (args[i].equals("-a") && i+1 < n)
				algName = args[++i];
			else if (args[i].equals("-p") && i+1 < n)
				problemName = args[++i];
			else if (args[i].equals("-s") && i+1 < n)
				populSize = Integer.parseInt(args[++i]);
			else if (args[i].equals("-m") && i+1 < n)
				mfe = Integer.parseInt(args[++i]);
			else if (args[i].equals("-r") && i+1 < n)
				seeds = Integer.parseInt(args[++i]);
			else usage();
		}

		//configure Analyzer
		Analyzer analyzer = new Analyzer()
			.withProblem(problemName)
			.includeAllMetrics()
			.showStatisticalSignificance()
			.showAggregate();

		//configure and run this experiment
		Executor executor = new Executor()
				.withProblem(problemName)
				.withProperty("populationSize", populSize)
				.withMaxEvaluations(mfe)
				.distributeOnAllCores();

		analyzer.addAll(algName, executor.withAlgorithm(algName).runSeeds(seeds));
		analyzer.printAnalysis();
	}
}

