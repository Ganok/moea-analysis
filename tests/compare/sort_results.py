#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import getopt

NUM_ALGOS = 13
NUM_METRICS = 7
NUM_IN = 5

try:
    opts, args = getopt.getopt(sys.argv[1:], 'hp:m:',['problem=','metric='])
except getopt.GetoptError as err:
    print(err)
    exit()

problem = 'Fonseca'
metric = 'GeneralizedSpread'
for opt, arg in opts:
    if opt in ('-p','--problem'):
        problem = arg
    elif opt in ('-m','--metric'):
        metric = arg
    elif opt in ('-o','--ofile'):
        outfile = arg
    else:
        print('Unrecognised command', opt)
        exit(2)


mem = {}
with open('./analysis/' + problem  + '_def_50.metrics','r') as metics:
    for i in range(NUM_ALGOS):
        namealgo = metics.readline()
        mem[namealgo] = {}
        for j in range(NUM_METRICS):
            namemetric = metics.readline().strip()
            mem[namealgo][namemetric] = {}
            for line in range(NUM_IN):
                l = metics.readline().split()
                mem[namealgo][namemetric][l[0]] = ''.join(l[1:])

res = []
for algo, dic in mem.items():
    res.append((algo, dic[metric + ':']['Median:']))

for (name,value) in sorted(res, key=lambda x:x[1]):
    print(name[:-1], value)

