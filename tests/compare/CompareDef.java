import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.moeaframework.Executor;
import org.moeaframework.Analyzer;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BoxAndWhiskerRenderer;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import org.jfree.chart.ChartUtilities;

public class CompareDef {

    private static void usage() {
        System.err.println("Usage: CompareDef " +
                "-m MAX_FUNCS_EVAL -r NUMBER_SEEDS ");
        System.exit(-1);
    }

    private static void printSolutions (List<NondominatedPopulation> result,
            String algName, String problemName, int seeds) {

        String objSol = "";
        String variableSol= "";
        boolean printvar = true, printobj = true;

        for (NondominatedPopulation pop : result) {
            for (Solution solution : pop) {
                if (solution.getNumberOfVariables() > 3) {
                    printvar = false;
                    break;
                }

                variableSol +=
                    String.valueOf(EncodingUtils.getReal(solution.getVariable(0)));
                for (int i = 1; i < solution.getNumberOfVariables(); ++i) {
                    variableSol += "\t" +
                        String.valueOf(EncodingUtils.getReal(solution.getVariable(i)));
                }
                variableSol += "\n";
            }
        }

        for (NondominatedPopulation pop : result) {
            for (Solution solution : pop) {
                if (solution.getNumberOfObjectives() > 3) {
                    printobj = false;
                    break;
                }

                objSol += String.valueOf(solution.getObjective(0));
                for (int i = 1; i < solution.getNumberOfObjectives(); ++i) {
                    objSol += "\t" + String.valueOf(solution.getObjective(i));
                }
                objSol += "\n";
            }
        }

        if (printobj) {
            try( PrintWriter out = new PrintWriter("./tests/compare/sets/" +
                        problemName + "_" + algName + "_def_obj_" +
                        String.valueOf(seeds) + ".sets") ){
                out.println(objSol);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (printvar) {
            try( PrintWriter out = new PrintWriter("./tests/compare/sets/" +
                        problemName + "_" + algName + "_def_var_" +
                        String.valueOf(seeds) + ".sets" ) ){
                    out.println(variableSol);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void saveBoxplots(Analyzer problem, String problemName,
            int seeds) {
        Analyzer.AnalyzerResults results = problem.getAnalysis();
        HashMap<String, DefaultBoxAndWhiskerCategoryDataset > datasetmap =
            new HashMap<String, DefaultBoxAndWhiskerCategoryDataset>();

        for (String namealgo : results.getAlgorithms()) {
            //produces nicer boxplots
            if (namealgo.equals("VEGA")) continue;

            Analyzer.AlgorithmResult algores = results.get(namealgo);
            for (String nameindicator : algores.getIndicators()) {
                Analyzer.IndicatorResult indres = algores.get(nameindicator);
                List<Double> values = new ArrayList<Double>();
                for(double d : indres.getValues()) values.add(d);

                if (!datasetmap.containsKey(nameindicator)) {
                    datasetmap.put(nameindicator,
                            new DefaultBoxAndWhiskerCategoryDataset());
                }
                datasetmap.get(nameindicator).add(values, "",
                        namealgo.equals("IBEA-JMetal") ? "IBEA" :
                        (namealgo.equals("OMOPSO") ? "omopso" : namealgo));
            }
        }

        Analyzer.AlgorithmResult algores = results.get("NSGAII");
        for (String nameindicator : algores.getIndicators()) {
            CategoryAxis xAxis = new CategoryAxis("Algorithms");
            xAxis.setLowerMargin(xAxis.getLowerMargin() * 0.2);
            xAxis.setUpperMargin(xAxis.getUpperMargin() * 0.2);
            NumberAxis yAxis = new NumberAxis(nameindicator + " Value");
            BoxAndWhiskerRenderer renderer = new BoxAndWhiskerRenderer();
            renderer.setMeanVisible(false);
            renderer.setUseOutlinePaintForWhiskers(true);
            yAxis.setAutoRangeIncludesZero(false);
            CategoryPlot plot =
                new CategoryPlot(datasetmap.get(nameindicator), xAxis, yAxis,
                        renderer);
            JFreeChart chart = new JFreeChart(problemName + " " +
                    nameindicator, plot);

            File img = new File("./tests/compare/boxplots/" + problemName +
                    "_def_" + nameindicator + "_" + String.valueOf(seeds) +
                    ".png");
            try {
                ChartUtilities.saveChartAsPNG(img,chart,960,640);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        String[] algNames = {"NSGAII", "DBEA", "MOEAD", "GDE3", "OMOPSO",
            "SMPSO", "SPEA2", "VEGA", "eMOEA", "eNSGAII", "Abyss", "PAES",
            "PESA2", "MOCell", "CellDE", "IBEA-JMetal", "CMA-ES", "NSGAIII"};
        String[] problemNames = {"Schaffer", "Schaffer2", "Fonseca",
            "Fonseca2", "Kursawe", "OKA1", "OKA2", "ZDT1", "ZDT2","ZDT3",
            "ZDT4", "ZDT5", "ZDT6", "DTLZ1_2", "DTLZ2_2", "DTLZ3_2",
            "DTLZ4_2", "DTLZ7_2", "DTLZ1_3", "DTLZ2_3", "DTLZ3_3", "DTLZ4_3",
            "DTLZ7_3", "WFG1_2", "WFG2_2", "WFG3_2", "WFG4_2", "WFG5_2",
            "WFG6_2", "WFG7_2", "WFG8_2", "WFG9_2", "WFG1_3", "WFG2_3",
            "WFG3_3", "WFG4_3", "WFG5_3", "WFG6_3", "WFG7_3", "WFG8_3",
            "WFG9_3"};

        Analyzer[] problems = new Analyzer[problemNames.length];
        int seeds = 100, mfe = 10000;

        //parse arguments to set options
        int n = args.length;
        for (int i = 0; i < n; ++i) {
            if (args[i].equals("-m") && i+1 < n)
                mfe = Integer.parseInt(args[++i]);
            else if (args[i].equals("-r") && i+1 < n)
                seeds = Integer.parseInt(args[++i]);
            else usage();
        }

        // initialize analyzers
        for (int i = 0; i < problemNames.length; ++i) {
            problems[i] = new Analyzer()
                .withProblem(problemNames[i])
                .includeGenerationalDistance()
                .includeInvertedGenerationalDistance()
                .includeSpacing()
                .includeGeneralizedSpread()
                .includeR2()
                .includeAdditiveEpsilonIndicator()
                .includeHypervolume()
                .showStatisticalSignificance();
        }

        // store execution time of algorithms
        HashMap<String,HashMap<String,Double>> avgTime =
            new HashMap<String,HashMap<String,Double>>();

        // evaluate each algorithm for each problem
        for (String algName : algNames) {
            Properties properties = new Properties();

            try {
                File propfile = new File("./tests/compare/alg_config/" +
                        algName + "_def.config");

                if (propfile.exists() && !propfile.isDirectory()) {
                    FileInputStream inp = new FileInputStream(propfile);
                    properties.load(inp);
                    inp.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            for (int i = 0; i < problemNames.length; ++i) {
                // skip problems that do not support binary variables
                if (problemNames[i].equals("ZDT5") &&
                        (algName.equals("GDE3") || algName.equals("MOEAD") ||
                        algName.equals("SMPSO") || algName.equals("Abyss") ||
                        algName.equals("CMA-ES") || algName.equals("OMOPSO") ||
                        algName.equals("CellDE"))) continue;

                if ((problemNames[i].equals("Fonseca") &&
                    algName.equals("CMA-ES")) ||
                    (problemNames[i].contains("WFG") &&
                     problemNames[i].contains("_3") &&
                     algName.equals("eMOEA")) ||
                    (problemNames[i].contains("WFG") &&
                     problemNames[i].contains("_3") &&
                     algName.equals("CMA-ES")) ||
                    (problemNames[i].contains("WFG") &&
                     problemNames[i].contains("_3") &&
                     algName.equals("eNSGAII"))) continue;

                Executor exec = new Executor()
                        .withProblem(problemNames[i])
                        .withMaxEvaluations(mfe)
                        .withAlgorithm(algName)
                        .distributeOnAllCores();

                // loop through set properties and set them
                Enumeration<?> e = properties.propertyNames();
                while (e.hasMoreElements()) {
                    String key = (String)e.nextElement();
                    exec.withProperty(key,properties.getProperty(key));
                }

                long start = System.nanoTime();
                List<NondominatedPopulation> result = exec.runSeeds(seeds);
                long elapsedTime = System.nanoTime() - start;
                double seconds = ((double)elapsedTime / 1e9);
                if (!avgTime.containsKey(problemNames[i]))
                    avgTime.put(problemNames[i],
                            new HashMap<String,Double>());
                avgTime.get(problemNames[i]).put(algName, seconds / seeds);

                printSolutions(result, algName, problemNames[i], seeds);
                problems[i].addAll(algName, result);

                System.out.println(algName + " " + problemNames[i] +
                        " completed");
            }
        }

        // save analysis
        for (int i = 0; i < problemNames.length; ++i) {

            saveBoxplots(problems[i], problemNames[i], seeds);

            // save file analysis
            File f = new File("./tests/compare/analysis/" + problemNames[i] +
                    "_def_" + String.valueOf(seeds) + ".metrics");
            try {
                problems[i].saveAnalysis(f);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // save file average elapsed time
            try( PrintWriter out = new PrintWriter("./tests/compare/time/" +
                        problemNames[i] + "_def_" + String.valueOf(seeds) +
                        ".time") ){
                for (Map.Entry<String,Double> entry :
                        avgTime.get(problemNames[i]).entrySet()) {
                    out.println(entry.getKey() + " " + entry.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}

